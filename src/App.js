import React from 'react';
import './App.css';
import { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


class App extends Component {
  render (){
  return (
    <div className="App">
      <div className="container">
        <div className="row">
         <div id="Smallchat">
        <div className="Layout Layout-open Layout-expand Layout-right">
          <div className="Messenger_messenger">
            <div className="Messenger_header">
              <h4 className="Messenger_prompt">How can we help you?</h4> <span className="chat_close_icon"><i className="fa fa-window-close" aria-hidden="true"></i></span> </div>
            <div className="Messenger_content">
              <div className="Messages">
                <div className="Messages_list"></div>
              </div>
              <div className="Input Input-blank">
                <form>
                        <div className="form-group row">
                            <label for="name" className="col-md-2 col-form-label">Name</label>
                            <div className="col-md-10">
                                <input type="text" className="form-control" id="name" name="name" placeholder="Name" />
                            </div>
                        </div>
    
                        <div className="form-group row">
                            <label for="emailid" className="col-md-2 col-form-label">Email</label>
                            <div className="col-md-10">
                                <input type="email" className="form-control" id="emailid" name="emailid" placeholder="Email" />
                            </div>
                        </div>
                        
                        <div className="form-group row">
                            <label for="telnum" className="col-12 col-md-2 col-form-label">Contact No.</label>
                            <div className="col-5 col-md-3">
                                <input type="tel" className="form-control" id="areacode" name="areacode" placeholder="Area Code" />
                            </div>
                            <div className="col-7 col-md-7">
                                <input type="tel" className="form-control" id="telnum" name="telnum" placeholder="Telephone Number" />
                            </div>
                        </div>
    
                        <div className="form-group row">
                            <div className="offset-md-2 col-md-10">
                                <button type="submit" className="btn btn-primary">
                                    <i className="fa fa-whatsapp" aria-hidden="true"><a href="https://api.whatsapp.com/send?phone=15551234567">Chat with Us</a></i>
                                </button>
                            </div>
                        </div>
                        
                    </form>
              </div>
            </div>
          </div>
        </div>
        <div className="chat_on"> <span className="chat_on_icon"><i className="fa fa-comments" aria-hidden="true"></i></span> </div>
      </div>
        </div>
    </div>
    </div>
  );
  }
}

export default App;
